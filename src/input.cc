/*
   Copyright (C) 1999/2000/2001  Alexandre Courbot.
   Copyright (C) 2016  Kai Sterker
   Part of the Adonthell Project <http://adonthell.nongnu.org>

   Adonthell is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Adonthell is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Adonthell.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * @file   input.cc
 * @author Alexandre Courbot <alexandrecourbot@linuxgames.com>
 * @author Kai Sterker
 * 
 * @brief  Defines the input class.
 * 
 * 
 */


#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <SDL.h>
#include "input.h"

u_int8 * input::keystate=NULL;
u_int8 * input::p_keystate=NULL;
bool input::text_input = false;
s_int32 input::keystatelength;

u_int16 input::mouse_posx, input::mouse_posy;
bool input::mouse_button[3];
std::queue<std::string> input::input_queue;

SDL_GameController *input::controller = NULL;

void input::init()
{
  const u_int8 *state = SDL_GetKeyboardState(&keystatelength);
  keystate=new u_int8[keystatelength];
  memcpy(keystate, state, keystatelength);

  //  set_keyboard_mode(MODE_STATE);
  p_keystate=new u_int8[keystatelength];
  memset(p_keystate, 0, keystatelength);

  controller = init_controller();
}

SDL_GameController *input::init_controller()
{
	for (int i = 0; i < SDL_NumJoysticks(); ++i)
	{
		if (SDL_IsGameController(i))
		{
			SDL_GameController *controller = SDL_GameControllerOpen(i);
			if (controller)
			{
				return controller;
			}
			else
			{
				std::cerr << "Failed to open controller " << SDL_GameControllerNameForIndex(i) << ": " << SDL_GetError() << std::endl;
			}
		}
	}
	return NULL;
}

void input::shutdown()
{
	if(controller != NULL)
	{
		SDL_GameControllerClose(controller);
	}
	delete[] p_keystate;
}

void input::update()
{
	static SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_KEYDOWN:
			{
				SDL_Scancode idx = event.key.keysym.scancode;
				if (!event.key.repeat)
				{
					keystate[idx]=1;
					p_keystate[idx]++;

					if (text_input)
					{
						if (idx == SDL_SCANCODE_BACKSPACE ||
							idx == SDL_SCANCODE_DELETE ||
							idx == SDL_SCANCODE_RETURN)
						{
							input_queue.push(std::string(1, (char)event.key.keysym.sym));
						}
					}
				}
				break;
			}
			case SDL_KEYUP:
			{
				SDL_Scancode idx = event.key.keysym.scancode;
				keystate[idx]=0;
				break;
			}
			case SDL_TEXTINPUT:
			{
				if (text_input)
				{
					input_queue.push(event.text.text);
				}
				break;
			}

			case SDL_CONTROLLERBUTTONDOWN:
			{
				SDL_Scancode idx = map_controller_button((SDL_GameControllerButton) event.cbutton.button);
				if (idx != SDL_SCANCODE_UNKNOWN)
				{
					keystate[idx] = 1;
					p_keystate[idx]++;
				}
				break;
			}
			case SDL_CONTROLLERBUTTONUP:
			{
				SDL_Scancode idx = map_controller_button((SDL_GameControllerButton) event.cbutton.button);
				if (idx != SDL_SCANCODE_UNKNOWN)
				{
					keystate[idx] = 0;
				}
				break;
			}
			case SDL_CONTROLLERDEVICEADDED:
			{
				if (controller == NULL)
				{
					controller = init_controller();
				}
				break;
			}
			case SDL_CONTROLLERDEVICEREMOVED:
			{
				if (controller != NULL)
				{
					SDL_Joystick *stick = SDL_GameControllerGetJoystick(controller);
					if (event.cdevice.which == SDL_JoystickInstanceID(stick))
					{
						SDL_GameControllerClose(controller);
						controller = NULL;
					}
				}
				break;
			}
		}
	}
}

SDL_Scancode input::map_controller_button(const SDL_GameControllerButton & button)
{
	switch(button)
	{
		case SDL_CONTROLLER_BUTTON_DPAD_UP:
		{
			return SDL_SCANCODE_UP;
		}
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		{
			return SDL_SCANCODE_DOWN;
		}
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		{
			return SDL_SCANCODE_LEFT;
		}
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		{
			return SDL_SCANCODE_RIGHT;
		}
		case SDL_CONTROLLER_BUTTON_A:
		{
			return SDL_SCANCODE_RETURN;
		}
		case SDL_CONTROLLER_BUTTON_B:
		{
			return SDL_SCANCODE_ESCAPE;
		}
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
		{
			return SDL_SCANCODE_PAGEUP;
		}
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
		{
			return SDL_SCANCODE_PAGEDOWN;
		}
		default:
		{
			return SDL_SCANCODE_UNKNOWN;
		}
	}
}

bool input::is_pushed(SDL_Keycode key)
{
  bool ret;
  SDL_Scancode idx = SDL_GetScancodeFromKey(key);
  ret=keystate[idx];
  if((ret)&&(p_keystate[idx])) p_keystate[idx]--;
  return ret;
}

bool input::has_been_pushed(SDL_Keycode key)
{
  bool ret;
  SDL_Scancode idx = SDL_GetScancodeFromKey(key);
  ret=p_keystate[idx];
  if((ret)&&(!(--p_keystate[idx]))) keystate[idx]=0;
  return ret;
}

SDL_Keycode input::get_next_key()
{
  static SDL_Event event;
  static bool b;
  b=false;
  if(SDL_PeepEvents(&event,1,SDL_GETEVENT,SDL_KEYDOWN,SDL_KEYDOWN)==1)
    {
      b=true;
      if(p_keystate[event.key.keysym.scancode]) p_keystate[event.key.keysym.scancode]--;
      keystate[event.key.keysym.scancode]=0;
    }
  if (b) return(event.key.keysym.sym);
  return(-1);
}

std::string input::get_next_unicode()
{
	if (!input_queue.empty())
	{
		std::string utf8_result(input_queue.front());
		input_queue.pop();
		return utf8_result;
	}
	return "";
}

void input::start_text_input()
{
	while(!input_queue.empty()) input_queue.pop();
	SDL_StartTextInput();
	text_input = true;
}

void input::stop_text_input()
{
	clear_keys_queue();
	text_input = false;
	SDL_StopTextInput();
}

void input::clear_keys_queue()
{
  memset(p_keystate, 0, keystatelength);
}
